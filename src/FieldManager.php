<?php
/**
 * @file
 * Contains Drupal\page_title\PageTitleConfigManager.
 */

namespace Drupal\page_title;

use Drupal\Core\Entity\EntityManagerInterface;

/**
 * Class PageTitleConfigManager
 * @package Drupal\page_title
 */
class FieldManager {

  /**
   * @var EntityManagerInterface
   */
  var $entityManager;

  /**
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * @param $plugin
   * @param $type
   */
  public function setupFields($plugin, $type) {
    $entity_type = $plugin->getEntityType();
    $bundle = $type->id();

    // Setup the base field if it doesn't exist.
    $field_storage = $this->loadFieldStorageByName($entity_type);
    if (!$field_storage) {
      $field_storage = $this->entityManager->getStorage('field_storage_config')
        ->create([
          'field_name' => 'page_title',
          'entity_type' => $entity_type,
          'type' => 'string',
        ]);
      $field_storage->save();
    }

    // Setup the field instance if it doesn't exist.
    $field = $this->loadFieldByName($entity_type, $bundle);
    if (!$field) {
      $field = $this->entityManager->getStorage('field_config')
        ->create([
          'field_storage' => $field_storage,
          'bundle' => $bundle,
          'label' => t('Page Title'),
          'settings' => [],
        ]);
      $field->save();

      // @todo, would factor this out for unit testing, but it seems to be a lot
      // of boilerplate.
      entity_get_form_display($entity_type, $bundle, 'default')
        ->setComponent('page_title', [
          'enabled' => TRUE,
        ])
        ->save();
    }

  }

  /**
   * @param $plugin
   * @param $type
   */
  public function tearDownFields($plugin, $type) {
    if ($field = $this->loadFieldByName($plugin->getEntityType(), $type->id())) {
      $field->delete();
    }
  }

  /**
   * @param $entity_type
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function loadFieldStorageByName($entity_type) {
    $storage = $this->entityManager->getStorage('field_storage_config');
    return $storage->load($entity_type . '.' . 'page_title');
  }

  /**
   * @param $entity_type
   * @param $bundle
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function loadFieldByName($entity_type, $bundle) {
    $storage = $this->entityManager->getStorage('field_config');
    return $storage->load($entity_type . '.' . $bundle . '.' . 'page_title');
  }

}
