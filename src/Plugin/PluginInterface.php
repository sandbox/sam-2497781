<?php

/**
 * @file
 * Contains Drupal\page_title\Plugin\PageTitlePluginInterface.
 */

namespace Drupal\page_title\Plugin;

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Interface for page title plugins.
 */
interface PluginInterface {

  /**
   * Derive an entity from a route.
   *
   * @param RouteMatchInterface $route_match
   *   The route matched by the current page.
   *
   * @return object|false
   */
  public function deriveEntityFromRoute(RouteMatchInterface $route_match);

}
