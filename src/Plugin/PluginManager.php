<?php

/**
 * @file
 * Contains Drupal\page_title\Plugin\PluginManager.
 */

namespace Drupal\page_title\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The page title plugin manager.
 */
class PluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/PageTitle', $namespaces, $module_handler, 'Drupal\page_title\Plugin\PluginInterface', 'Drupal\page_title\Annotation\PageTitle');
    $this->setCacheBackend($cache_backend, 'page_title_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionByFormId($form_id) {
    foreach ($this->getDefinitions() as $definition) {
      if ($definition['bundle_form_id'] === $form_id) {
        return $this->createInstance($definition['id']);
      }
    }
    return FALSE;
  }

}
