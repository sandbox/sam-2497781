<?php

/**
 * @file
 * Contains Drupal\page_title\Plugin\PageTitlePluginBase.
 */

namespace Drupal\page_title\Plugin;

use Drupal\Core\Plugin\PluginBase as CorePluginBase;

/**
 * A base for the page title plugins.
 *
 * Plugins exist to provide certain metadata about each entity that needs to
 * be supported and also provides the foundation for expanding on entity type
 * specific requirements.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface  {

  /**
   * Get the entity type that this plugin backs.
   */
  public function getEntityType() {
    return $this->getPluginId();
  }

}
