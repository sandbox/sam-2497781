<?php

/**
 * @file
 * Contains Drupal\page_title\Plugin\page_title\Term.
 */

namespace Drupal\page_title\Plugin\PageTitle;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\page_title\Plugin\PluginBase;
use Drupal\taxonomy\TermInterface;

/**
 * @PageTitle(
 *   id = "taxonomy_term",
 *   bundle_form_id = "taxonomy_vocabulary_form"
 * )
 */
class Term extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function deriveEntityFromRoute(RouteMatchInterface $route) {
    $term = $route->getParameter('taxonomy_term');
    return $term instanceof TermInterface ? $term : FALSE;
  }

}
