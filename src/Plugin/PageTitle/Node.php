<?php

/**
 * @file
 * Contains Drupal\page_title\Plugin\page_title\Node.
 */

namespace Drupal\page_title\Plugin\PageTitle;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\page_title\Plugin\PluginBase;

/**
 * @PageTitle(
 *   id = "node",
 *   bundle_form_id = "node_type_edit_form"
 * )
 */
class Node extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function deriveEntityFromRoute(RouteMatchInterface $route) {
    $node = $route->getParameter('node');
    return $node instanceof NodeInterface ? $node : FALSE;
  }

}
