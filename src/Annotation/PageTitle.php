<?php
/**
 * @file
 * Contains \Drupal\page_title\Annotation\PageTitle.
 */

namespace Drupal\page_title\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a flavor item annotation object.
 *
 * Plugin Namespace: Plugin\icecream\flavor
 *
 * @see \Drupal\icecream\Plugin\IcecreamManager
 * @see plugin_api
 *
 * @Annotation
 */
class PageTitle extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Bundle form ID.
   *
   * @var string
   */
  public $bundle_form_id;

}
